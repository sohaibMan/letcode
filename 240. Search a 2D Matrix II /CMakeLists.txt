cmake_minimum_required(VERSION 3.26)
project(240__Search_a_2D_Matrix_II_)

set(CMAKE_CXX_STANDARD 17)

add_executable(240__Search_a_2D_Matrix_II_ main.cpp)
